import React from 'react';

const Page404 = (props) => {
  return (
    <div className="container d-flex justify-content-center align-items-center" id="page404">
      <h1>Page 404 | Not found</h1>
    </div>
  )
}

export default Page404;