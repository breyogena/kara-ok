import React, {useContext} from 'react';
import {Route, Redirect} from 'react-router-dom';
import {AppContext} from './AppProvider';
import Page404 from './Page404';

const PrivateRoute = (props) => {

	const [authUser] = useContext(AppContext)

  return (
    <Route>
    	{
    		authUser.isAuth ? props.children : <Page404 />
    		// or you can use page 403 so that the route is still the same
    	}
    </Route>
  )
}

export default PrivateRoute;