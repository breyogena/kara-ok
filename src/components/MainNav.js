import React, {useContext, useEffect, useState} from 'react';
import {NavLink, Link} from 'react-router-dom';
import Logo from './../images/logo.png';
import {AppContext} from './../AppProvider';

const MainNav = () => {

	const [authUser, setAuthUser] = useContext(AppContext)
      

	return (
	
			<nav className="navbar navbar-expand-lg navbar-light shadow-sm bg-white rounded text-uppercase">
				<ul className="navbar-nav">
						<Link className="navbar-brand" to="/">
							<img src={Logo} alt="logo" />
						</Link>
				</ul>
				<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
					<span className="navbar-toggler-icon"></span>
				</button>
				<div className="collapse navbar-collapse" id="navbarNav">
					<ul className="navbar-nav mr-auto">
						<li className="nav-item">
							<NavLink className="nav-link" exact to="/">
								<svg width="1.2rem" height=".9rem" viewBox="0 0 16 16" className="bi bi-house-fill mr-.5" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
									<path fillRule="evenodd" d="M8 3.293l6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293l6-6zm5-.793V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
									<path fillRule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"/>
								</svg> 
								Home
							</NavLink>
						</li>
						<li className="nav-item">
							<NavLink className="nav-link" exact to="/rooms">
								<svg width="1.2rem" height=".9rem" viewBox="0 0 16 16" className="bi bi-door-open-fill mr-.5" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  									<path fillRule="evenodd" d="M1.5 15a.5.5 0 0 0 0 1h13a.5.5 0 0 0 0-1H13V2.5A1.5 1.5 0 0 0 11.5 1H11V.5a.5.5 0 0 0-.57-.495l-7 1A.5.5 0 0 0 3 1.5V15H1.5zM11 2v13h1V2.5a.5.5 0 0 0-.5-.5H11zm-2.5 8c-.276 0-.5-.448-.5-1s.224-1 .5-1 .5.448.5 1-.224 1-.5 1z"/>
								</svg>
								Rooms
							</NavLink>
						</li>

						{
							authUser.isAdmin ?
							<li className="nav-item dropdown">
								<Link className="nav-link dropdown-toggle" to="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i className="fas fa-user-cog mr-1"></i>
									Admin Control
								</Link>
								<div className="dropdown-menu" aria-labelledby="navbarDropdown">
									<Link className="dropdown-item" to="/rooms/add-room">
										<i className="fas fa-plus mr-1"></i>
										Add new Room
									</Link>
								</div>
							</li>
							: "" 
						}
						
					</ul>
					
					<ul className="navbar-nav ml-auto">

						<li className="nav-item">
							<NavLink className="nav-link" to="/reservations">
								<svg width="1.2rem" height=".9rem" viewBox="0 0 16 16" className="bi bi-collection-fill mr-1" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  									<path d="M0 13a1.5 1.5 0 0 0 1.5 1.5h13A1.5 1.5 0 0 0 16 13V6a1.5 1.5 0 0 0-1.5-1.5h-13A1.5 1.5 0 0 0 0 6v7z"/>
  									<path fillRule="evenodd" d="M2 3a.5.5 0 0 0 .5.5h11a.5.5 0 0 0 0-1h-11A.5.5 0 0 0 2 3zm2-2a.5.5 0 0 0 .5.5h7a.5.5 0 0 0 0-1h-7A.5.5 0 0 0 4 1z"/>
								</svg>
								Reservations
							</NavLink>
						</li>





						{
							!authUser.isAuth ? 
							<>
								<li className="nav-item">
									<NavLink className="nav-link" to="/login">Login</NavLink>
								</li>

								<li className="nav-item">
									<NavLink className="nav-link" to="/register">Register</NavLink>
								</li>
							</>

							: 
								<li className="nav-item">
									<NavLink to="/logout" className="nav-link" >Logout</NavLink>
								</li>
						}





					</ul>
				</div>
			</nav>
	
	)
}

export default MainNav;



