import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import RoomCard from './room-partials/RoomCard';
import Loading from './../images/loading.gif';

const RoomSingle = (props) => {
 	const {id} = useParams();

    // query 
    const [ room, setRoom ] = useState({});
    const [ isLoading, setIsLoading ] = useState(true);

    useEffect(()=>{
        fetch(`https://karaokdb.herokuapp.com/rooms/${id}`)
        .then( response => response.json())
        .then( data => {
            setRoom(data)
            setIsLoading(false)
        })
    },[])

  return (
    <div>
    	<div className="container">
    		<div className="row">
    			<div className="col-12 col-md-8 col-lg-6 mx-auto">
                {
                    isLoading ?
    				<div className="container">
                        <div className="row">
                            <div className="col-12" id="loading">
                                <img src={Loading} alt="" />
                                
                            </div>
                            
                        </div>
                    </div>
                    :
                    <RoomCard room={room} />
                }
    			</div>
    		</div>
    	</div>
    </div>
  )
}

export default RoomSingle;