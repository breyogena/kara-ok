import React, {useState} from 'react';
// import AlertMessage from './main-partials/AlertMessage';
import {Link} from 'react-router-dom';
import './../css/Style.css';
import swal from 'sweetalert';
import {Redirect} from 'react-router-dom';


const Register = (props) => {

	const [user, setUser] = useState({
        fullname: "",
        email: "",
        password: "",
        confirmPassword: ""
    })

    // const [error, setError] = useState({
    //     hasError : false,
    //     color: "",
    //     message: ""
    // })

    const [isSuccess, setIsSuccess] = useState(false);

    const [isLoading, setIsLoading] = useState(false);

    if(isSuccess) {
		return <Redirect to="/login" />
	}

    const handleChange = e => {
        setUser({
            ...user,
            [e.target.name] : e.target.value
        })
    }

    const handleSubmit = e => {
        e.preventDefault();
        setIsLoading(true)

        fetch("https://karaokdb.herokuapp.com/users/register", {
            // we need to tell to our backend that we send post request
            method: "post",
            body: JSON.stringify(user),
            headers: {
                'Content-Type' : 'application/json'
            }
        })
        .then( response => {
            console.log(response.status);
            if(response.status === 400){ 
            	swal({
            		title: "Failed to Register!",
            		text: "Please, check your Credentials",
            		icon: "error",
            		button: "Got it",
            		timer: 3000
            	});
            	setIsLoading(false)
            } else {
            	swal({
            		title: "You are now registered!",
            		icon: "success",
            		button: "Got it",
            		timer: 3000
            	});
               setIsSuccess(true)
            }

            return response.json()
        })
        .then( data => {
            console.log(data)
        })
    }


  return (
    <div>
	  	<div className="container-fluid" id="register-page">
	  		<div className="row">
	  			<div className="col-12 col-md-6 my-5 my-md-0 d-flex justify-content-center align-items-center">
	  				<div className="card shadow bg-white w-75">
	  					<div className="card-header font-weight-bold">
		  					<svg width="2rem" height="2rem" viewBox="0 0 16 16" className="bi bi-person-plus-fill mr-1" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
		  						<path fillRule="evenodd" d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm7.5-3a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
		  					</svg> 
	  						 Register
	  					</div>
	  					<div className="card-body">
	  						<form onSubmit={handleSubmit}>
	  						


	  							<div className="form-group">
	  								<label htmlFor="name" className="font-weight-bold">Full Name:</label>
	  								<div className="input-group">
	  									<div className="input-group-text">
	  										<svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-person-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	  											<path fillRule="evenodd" d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
	  										</svg>
	  									</div>
	  									<input 
	  										type="text" 
	  										className="form-control" 
	  										id="fullname" 
	  										placeholder="Full Name" 
	  										name="fullname" 
	  										onChange={handleChange}
	  									/>
	  								</div>
	  								
	  							</div>
	  							<div className="form-group">
	  								<label htmlFor="email" className="font-weight-bold">Email:</label>
	  								<div className="input-group-prepend">
	  									<div className="input-group-text">
	  										<svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-envelope-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	  											<path fillRule="evenodd" d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555zM0 4.697v7.104l5.803-3.558L0 4.697zM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757zm3.436-.586L16 11.801V4.697l-5.803 3.546z"/>
	  										</svg>
	  									</div>
	  									<input 
	  										type="email" 
	  										className="form-control" 
	  										id="email" 
	  										placeholder="Email" 
	  										name="email" 
	  										onChange={handleChange}
	  									/>
	  								</div>
	  								
	  							</div>
	  							<div className="form-group">
	  								<label htmlFor="password" className="font-weight-bold">Password:</label>
	  								<div className="input-group-prepend">
	  									<div className="input-group-text">
	  										<svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-lock-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	  											<path d="M2.5 9a2 2 0 0 1 2-2h7a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-7a2 2 0 0 1-2-2V9z"/>
	  											<path fillRule="evenodd" d="M4.5 4a3.5 3.5 0 1 1 7 0v3h-1V4a2.5 2.5 0 0 0-5 0v3h-1V4z"/>
	  										</svg>
	  									</div>
	  									<input 
	  										type="password" 
	  										className="form-control" 
	  										id="password" 
	  										placeholder="Password" 
	  										name="password" 
	  										onChange={handleChange}
	  									/>
	  								</div>
	  							</div>
	  							<div className="form-group">
	  								<label htmlFor="confirmPassword" className="font-weight-bold">Confirm Password:</label>
	  								<div className="input-group-prepend">
	  									<div className="input-group-text">
	  										<svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-lock-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	  											<path d="M2.5 9a2 2 0 0 1 2-2h7a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-7a2 2 0 0 1-2-2V9z"/>
	  											<path fillRule="evenodd" d="M4.5 4a3.5 3.5 0 1 1 7 0v3h-1V4a2.5 2.5 0 0 0-5 0v3h-1V4z"/>
	  										</svg>
	  									</div>
	  									<input 
	  										type="password" 
	  										className="form-control" 
	  										id="confirmPassword" 
	  										placeholder="Confirm Password" 
	  										name="confirmPassword" 
	  										onChange={handleChange}
	  									/>
	  								</div>
	  							</div>
	  							<small className="d-block">Already have an account? <Link to="/login">Sign in.</Link></small>
	  							<button 
		  							className="btn btn-primary mt-2"
		  							disabled={isLoading}
		  						>
		  						{
		  							isLoading ?
		  							"Register..." :
		  							"Register"
		  						}
		  						</button>
	  						</form>
	  					</div>
	  				</div>
	  			</div>
	  			<div className="col-12 col-md-6 d-none d-md-block" id="register-image">
	  				
	  				
	  			</div>
	  		</div>
	  	</div>
	</div>
  )
}

export default Register;