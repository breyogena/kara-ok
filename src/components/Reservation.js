import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import Loading from './../images/loading.gif';

const Reservation = () => {

    const [reservations, setReservations] = useState([])
    const [isLoading, setIsLoading] = useState(true)

    useEffect(()=>{
        fetch("https://karaokdb.herokuapp.com/reservations", {
            headers: {
                "Authorization" : `Bearer ${localStorage['appState']}`
            }
        })
        .then( response => response.json())
        .then( data => {
            setReservations(data)
            setIsLoading(false)
        })
    },[])


    let reservationList = reservations.map(reservation => {
        return (
           <li className="list-group-item" key={reservation._id}>
                <Link to={`/reservations/${reservation._id}`}>
                    {reservation._id}
                    <span 
                        className="badge badge-warning ml-2"
                        >
                    {
                        reservation.isComplete? "Completed" : "Pending"
                    }
                    </span>
                </Link>
            </li> 
        )
    })

    return (
    <div className="container py-5" id="reservation">
    	<div className="row">
    		<div className="col-12 col-md-8 col-lg-6 mx-auto">
                <h1 className="text-uppercase text-center">All Reservations</h1>
    			
                {
                    !isLoading ?
        			<ul className="list-group">
        				{reservationList}
        			</ul>
                    : 
                    <div className="container">
                        <div className="row">
                            <div className="col-12" id="loading">
                                <img src={Loading} alt="" />
                                
                            </div>
                            
                        </div>
                    </div>
                }
    		</div>
    	</div>
    </div>
  )
}

export default Reservation;