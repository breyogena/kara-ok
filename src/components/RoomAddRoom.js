import React from 'react';
import RoomAddRoomForm from './room-partials/RoomAddRoomForm';

const RoomAddRoom = (props) => {
  return (
    <div className="container my-5">
    	<div className="row">
    		<div className="col-12 col-md-8 col-lg-6 mx-auto">
    			
    			<RoomAddRoomForm />

    		</div>
    	</div>
    </div>
  )
}

export default RoomAddRoom;