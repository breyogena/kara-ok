import React, {useState, useEffect, useContext} from 'react';
import RoomCard from './room-partials/RoomCard';
// import {Redirect} from 'react-router-dom';
import Loading from './../images/loading.gif';
import {AppContext} from './../AppProvider';

const Room = (props) => {

		const [authUser, setAuthUser] = useContext(AppContext)

		const [rooms, setRooms ] = useState([]);
		const [isRedirect, setIsRedirect] = useState(false)
		const [deletedRoom, setDeletedRoom] = useState({})
		const [isLoading, setIsLoading] = useState(true);

		useEffect( () => {
			let appState = localStorage["appState"];

			if(appState) {
				fetch("https://karaokdb.herokuapp.com/users/profile",{
					headers: {
						"Authorization": `Bearer ${appState}`
					}
				})
				.then( response => response.json())
				.then( data => {

					if(data._id){

						setAuthUser({
							isAuth: true,
							_id: data._id,
							fullname: data.fullname,
							email: data.email,
							isAdmin: data.isAdmin
						})
					}
				})
			}
		}, []);


		useEffect(() => {
			fetch("https://karaokdb.herokuapp.com/rooms")
			.then( response => { return response.json()})
			.then( rooms => { 
				if(rooms){
					setIsLoading(false)
				}
				setRooms(rooms)
			})
		},[]);

		useEffect(()=>{
			if(deletedRoom) {
				setRooms(rooms.filter( room => {
					return room._id !== deletedRoom._id
				}))

			}
		},[deletedRoom]);

		let roomList = rooms.map( room => (
			<div className="col-12 col-md-4" key={room._id}>


				<RoomCard authUser={authUser} setDeletedRoom={setDeletedRoom} setIsRedirect={setIsRedirect}  room={room} />
			</div>
			));

		return (
			<div className="container my-5" id="room">
				<div className="row">

				{
					!isLoading ?
					[roomList]
					: 
					<div className="container">
						<div className="row">
							<div className="col-12" id="loading">
								<img src={Loading} alt="" />
								
							</div>
							
						</div>
					</div>
				}


				</div>
			</div>
			)
	}

export default Room;