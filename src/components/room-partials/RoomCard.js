import React, {useContext, useEffect, useState} from 'react';
import RoomBook from './RoomBook';
import RoomAdminControl from './RoomAdminControl';
import {Link} from 'react-router-dom';
import Loading from './../../images/loading.gif';
import {AppContext} from './../../AppProvider'

const RoomCard = ({room, withDescription, setDeletedRoom, setIsRedirect}) => {

	const [authUser, setAuthUser] = useContext(AppContext)

	useEffect( () => {
    let appState = localStorage["appState"];

    if(appState) {
      fetch("https://karaokdb.herokuapp.com/users/profile",{
        headers: {
          "Authorization": `Bearer ${appState}`
        }
      })
      .then( response => response.json())
      .then( data => {
        
        if(data._id){
          
          setAuthUser({
            isAuth: true,
            _id: data._id,
            fullname: data.fullname,
            email: data.email,
            isAdmin: data.isAdmin
          })
        }
      })
    }
  }, []);
	return (
		<div className="card my-3 shadow-lg ">
	
			<img src={`https://karaokdb.herokuapp.com/${room.image}`} alt="Image expired" className="card-img-top"  />

			<div className="card-body">




				<h5 className="card-title font-weight-bold text-uppercase mt-2">{room.name}</h5>


				{
					room.price <= 500  ? 
						<ul className="list-unstyled list-inline rating my-1" id="ratings">
							<li className="list-inline-item mr-0"><i className="fas fa-star amber-text font-weight-bold"></i></li>
							<li className="list-inline-item mr-0"><i className="fas fa-star amber-text font-weight-bold"></i></li>
							<li className="list-inline-item mr-0"><i className="fas fa-star amber-text font-weight-bold"></i></li>
							<li className="list-inline-item"><i className="fas fa-star-half-alt amber-text"></i></li>
							{
								!room.price > 400 || room.price < 500 ?
								
								<li className="list-inline-item"><p className="text-muted">3.5 (150+)</p></li>
								: 
								<li className="list-inline-item"><p className="text-muted">5.5 (200+)</p></li>


							}
						</ul>
						:
					
						<ul className="list-unstyled list-inline rating my-1" id="ratings">
							<li className="list-inline-item mr-0"><i className="fas fa-star amber-text font-weight-bold"> </i></li>
							<li className="list-inline-item mr-0"><i className="fas fa-star amber-text font-weight-bold"> </i></li>
							<li className="list-inline-item mr-0"><i className="fas fa-star amber-text font-weight-bold"></i></li>
							<li className="list-inline-item mr-0"><i className="fas fa-star amber-text font-weight-bold"></i></li>
							<li className="list-inline-item mr-0"><i className="fas fa-star amber-text font-weight-bold"></i></li>
							
							<li className="list-inline-item"><p className="text-muted ml-2">9.5 (800+)</p></li>
						</ul>
					
				}

				<p className="card-text">{room.description}</p> 

				<p className="card-text">&#8369; {room.price} | Per hour</p>
				 		
			</div>
			<div className="card-footer">
				{/*add to cart*/}

				{
					withDescription ?
					<p className="card-text">{room.description}</p> :
					<></>
				}

				{
					!authUser.isAdmin ? 
					<Link to={`/bookings/${room._id}`} className="btn btn-success my-1 w-100">Book now</Link>
					: ""
				}

				

				{
					authUser.isAdmin ?

					<RoomAdminControl setDeletedRoom={setDeletedRoom} id={room._id} setIsRedirect={setIsRedirect} />	
					: "" 
				}
					

	  		</div>
		</div>


	)
}

export default RoomCard;