import React, {useState} from 'react';
import InputGroup from './../main-partials/InputGroup';
import {Redirect} from 'react-router-dom';
import swal from 'sweetalert';

const RoomAddRoomForm = (props) => {
 const [isRedirect, setIsRedirect] = useState({
      latestId : "",
      success: false
   });

   const [room, setRoom] = useState({
      name: "",
      price: 0,
      description: "",
      image: ""

   });

   const [isLoading, setIsLoading] = useState(false);

   if(isRedirect.success) {
       return <Redirect to="/rooms" />

   }

   const handleChange = e => {
      setRoom({
         ...room,
         [e.target.name] : e.target.value
      })
   }

   const handleChangeFile = e => {
      setRoom({
         ...room,
         image: e.target.files[0]
      })
      console.log(e.target.files[0])
   }

   const handleSubmit = e => {
      e.preventDefault()

      let formData = new FormData();
      formData.append('name', room.name)
      formData.append('price', room.price)
      formData.append('description', room.description)
      formData.append('image', room.image)

      fetch('https://karaokdb.herokuapp.com/rooms', {
         method: "post",
         body: formData,
         headers: {
            "Authorization" : `Bearer ${localStorage['appState']}`
         }
      })
      .then( res => res.json())
      .then( data => {
         if(data._id) {
            setIsRedirect({
               latestId: data._id,
               success: true
            })
            swal({
               title: "Room Added Successfully!",
               icon: "success",
               button: "Ok",
               timer: 3000
            });
         } else {
            swal({
                  title: "Failed to add. Check your input, try again.",
                  icon: "error",
                  button: "Ok",
                  timer: 3000
               });
         }
         console.log(data)
      })
   }

  return (
      <form onSubmit={handleSubmit}>
         <InputGroup 
            name="name"
            displayName="Room Name: "
            type="text"
            handleChange={handleChange}
         />

         <InputGroup 
            name="price"
            displayName="Room Price: "
            type="number"
            handleChange={handleChange}
         />

         <InputGroup 
            name="image"
            displayName="Room Image: "
            type="file"
            handleChange={handleChangeFile}
         />
         <label htmlFor="description" className="font-weight-bold">Room Description: </label>
         <textarea 
            name="description" 
            id="description" 
            cols="30" 
            rows="5" 
            className="form-control"
            onChange={handleChange}
         ></textarea>
         
         <button 
            className="btn btn-primary my-3"
            disabled={isLoading}
         >
            {
               isLoading ?
               "Adding room" :
               "Add Room"
            }
         </button>
      </form>
  )
}

export default RoomAddRoomForm;