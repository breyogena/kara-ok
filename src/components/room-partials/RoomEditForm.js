import React, {useState, useEffect} from 'react';
import InputGroup from './../main-partials/InputGroup';
import {Redirect} from 'react-router-dom';
import swal from 'sweetalert';

const RoomEditForm = (props) => {
    const [room, setRoom] = useState({});

    const [isRedirect, setIsRedirect] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    const handleChange = e => {
        setRoom({
            ...room,
            [e.target.name] : e.target.value
        })
    }

    const handleChangeFile = e => {
        setRoom({
            ...room,
            image: e.target.files[0]
        })
    }

    const handleSubmit = e => {
        e.preventDefault()

        let formData = new FormData();
        let error = 0

        
        if(room.name && room.price && room.description !== ""){
            formData.append('name', room.name);
            formData.append('price', room.price);
            formData.append('description', room.description);

        } else {
            error++
            swal({
                    title: "INPUT FIELD IS REQUIRED",
                    icon: "error",
                    button: "Ok",
                    timer: 3000
            });

        }

        
        if(room.image) {
            formData.append('image', room.image);
        }
        fetch(`https://karaokdb.herokuapp.com/rooms/${room._id}`,{
            method: "put",
            headers: {
                'Authorization' : `Bearer ${localStorage['appState']}`
            },
            body: formData
        })
        .then( res => res.json())
        .then( data => {
            if(error === 0){
                console.log(data)
                setIsRedirect(true)
                swal({
                    title: "Room edited successfully",
                    icon: "success",
                    button: "Ok",
                    timer: 3000
                });
            }
        })
    }

    useEffect(()=> {
        setRoom({
            ...props.room,
            image: ""
        })
    }, [props.room])

    if(isRedirect) {
        return <Redirect to="/rooms" />
    }


  return (
    <form onSubmit={handleSubmit}>
    	<InputGroup 
    		type="text"
    		name="name"
    		displayName="Room Name:"
            value={room.name}
            handleChange={handleChange}
            
    	/>

    	<InputGroup 
    		type="number"
    		name="price"
    		displayName="Room Price:"
            value={room.price}
            handleChange={handleChange}
            
    	/>
    	<InputGroup 
    		type="file"
    		name="image"
    		displayName="Room Image:"
            handleChange={handleChangeFile}

    	/>

    	<label htmlFor="description" className="font-weight-bold">Room Description</label>
    	<textarea 
    		name="description" 
    		id="description" 
    		cols="30" 
    		rows="5"
            className="form-control"
            value={room.description}
            onChange={handleChange}
            
        
    	>  

        </textarea>
        <button 
            className="btn btn-primary mt-2"
            disabled={isLoading}
        >
        {
            isLoading ?
            "Editing..." :
            "Edit"
        }
        </button>
    </form>
  )
}

export default RoomEditForm;