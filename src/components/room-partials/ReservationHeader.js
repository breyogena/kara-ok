import React, {useState, useEffect, useContext} from 'react';
import {useParams, Redirect} from 'react-router-dom'
import {AppContext} from './../../AppProvider'
import swal from 'sweetalert';

const ReservationHeader = ({reservation}) => {

	const [authUser, setAuthUser] = useContext(AppContext)
	const [isRedirect, setIsRedirect] = useState(false);

	console.log(authUser)

	const { id } = useParams();

	const [isComplete, setIsComplete] = useState({
		isComplete: false
	})


	useEffect(() => {
		fetch(`https://karaokdb.herokuapp.com/reservations/${id}`, {
			headers: {
				"Authorization" : `Bearer ${localStorage["appState"]}`
			}
		})
		.then(response => response.json())
		.then( data => {
			setIsComplete(data)
		});
	},[])

	const handleChange = e => {
		if(e.target.value === "true") {
			setIsComplete({
				...isComplete,
				isComplete: true
			})
			
		} else {
			setIsComplete({
				...isComplete,
				isComplete: false
			})
		}
	}
	console.log(isComplete)

    const handleSubmit = e => {
    	e.preventDefault()

    	fetch(`https://karaokdb.herokuapp.com/reservations/${id}`, {
    		method: "put",
    		body: JSON.stringify(isComplete),
    		headers : {
    			"Content-Type" : "application/json",
    			'Authorization' : `Bearer ${localStorage['appState']}`
    		},
    	})
    	.then(res => res.json())
    	.then(data => {
    		if(data._id){
    			console.log(data)
    			setIsRedirect(true)
                swal({
                    title: "Reservation status updated successfully",
                    icon: "success",
                    button: "Ok",
                    timer: 3000
                });
    		}
    	})
    }	

    if(isRedirect) {
        return <Redirect to="/reservations" />
    }

    // console.log(startDate)

	return (
		
				<table className="table my-5">

					<tbody>
						{/*Transaction code*/}
						<tr>
							<td className="font-weight-bold">Reservation Code</td>
							<td>{reservation._id}</td>
						</tr>

						{/*customer*/}
						<tr>
							<td className="font-weight-bold">Customer Name</td>
							<td>{reservation.customerId.fullname}</td>
							
						</tr>

						{/*purchased date*/}
						<tr>
							<td className="font-weight-bold">Reservation date</td>
							{/*<td>{reservation.createdAt}</td>*/}
							{/*<td>{reservation.startDate}</td>*/}

							<td>{reservation.startDate}</td>
						</tr>

						{/*status*/}
						<tr>
							<td className="font-weight-bold">Status</td>
							{
								authUser && authUser.isAdmin ?
								<td>
									<select onChange={handleChange}>
	                        			<option value={false}>Pending</option>
	                        			<option value={true}>Complete</option>
	                    			</select> 
	                    			<button onClick={handleSubmit} className="btn btn-primary btn-sm ml-2 text-uppercase">Update</button>
								</td>
								: 
								<td>{reservation.isComplete ? "Completed" : "Pending"}</td>
							}

						</tr>
					</tbody>
				</table>
		
	)
}

export default ReservationHeader;