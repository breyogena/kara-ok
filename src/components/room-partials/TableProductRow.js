import React from 'react';

const TableProductRow = ({order, withAction}) => {
  	return (
		  	<tr>
			  	
			  	<td>{order.roomId.name}</td>

			  
			  	<td>&#8369;{order.price}</td>

			  	
			  	<td>{order.quantity}</td>

			  
			  	<td>{order.subtotal}</td>

			  	
		  	</tr>
  	)
}

export default TableProductRow;