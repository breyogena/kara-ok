import React, {useState, useEffect, useContext} from 'react';
import InputGroup from './../main-partials/InputGroup';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Moment from 'react-moment';
import {Redirect, Link} from 'react-router-dom';
import swal from 'sweetalert';
import {AppContext} from './../../AppProvider'

const RoomBook = ({room}) => {

  const [authUser, setAuthUser] = useContext(AppContext)

	  const [startDate, setStartDate] = useState(new Date());
    console.log(startDate)

    const [hours, setHours] = useState(0);
    const [isLoading, setIsLoading] = useState(false);

	  const [book, setBook] = useState({
      startDate: "",
      orders: []
    });

    const [isSuccess, setIsSuccess] = useState(false);

    if(isSuccess) {
      return <Redirect to="/reservations"/>
    }

    const handleSubmit = e => {
      e.preventDefault()
      fetch(`https://karaokdb.herokuapp.com/reservations`, {
        method: "post",
        body: JSON.stringify(book),
            headers: {
               "Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage['appState']}`
        }
      })
        .then(response => response.json())
        .then(data => {
            setIsLoading(false)
            setIsSuccess(true)
            swal({
                title: "Book successfully!",
                icon: "success",
                button: "Got it",
                timer: 3000
            });
            
        });
    }


    console.log(room._id)

    const handleChange = e => {
    	setBook({ 
        startDate: startDate.toDateString(),
        orders: [{
          roomId: room._id,

      		[e.target.name] : e.target.value 
        }],
    	})
      setHours({
        [e.target.name] : e.target.value 
      })

    }

    console.log(hours)
    console.log(startDate)

 


  return (
    <div>
    	<form onSubmit={handleSubmit}>
    		 <label className="font-weight-bold">Select Date of Reservation</label>
    			<DatePicker
    		 		selected={startDate}
    		 		onChange={date => setStartDate(date)}
    		 		timeInputLabel="Time:"
    		 		dateFormat="MM/dd/yyyy h:mm aa"
    		 		showTimeInput
    		 	/>
    		<InputGroup
    		  	type="number"
    		  	name="quantity"
    		  	displayName="How many hours?"
    		  	min="1"
    		  	max="12"
    		  	handleChange={handleChange}
    	  	/>
          <h4>Total: &#8369; {hours.quantity ? room.price * hours.quantity : "0"}</h4>

        {
          authUser.isAuth ?
    	  	<button className="btn btn-success w-100">Confirm Book</button>
          : 
          <Link to="/login" className="btn btn-success w-100">Login to book</Link>
        }       
    	</form>
  </div>
  )
}

export default RoomBook;