import React, {useState, useEffect} from 'react';
import RoomEditForm from './room-partials/RoomEditForm';
import RoomCard from './room-partials/RoomCard';
import {useParams} from 'react-router-dom';
import Loading from './../images/loading.gif';

const RoomEdit = (props) => {
  
    const { id } = useParams();

    const [room, setRoom] = useState({
        name: "",
        image: "",
        price: "",
        description: ""
    });

    const [isLoading, setIsLoading] = useState(true)

    useEffect(() =>{
        fetch(`https://karaokdb.herokuapp.com/rooms/${id}`)
        .then(response => response.json())
        .then(data => {
            setRoom(data)
            setIsLoading(false)
        });
    },[]);

  return (
    <div className="container my-5">
    	<div className="row">
    		<div className="col-12 col-md-8 col-lg-6 mx-auto">
    			<RoomEditForm room={room} />
    		</div>
    		<div className="col-12 col-md-8 col-lg-6 mx-auto">
    			{
                    !isLoading ?
                    <RoomCard room={room} />
                    : 
                    <div className="container">
                        <div className="row">
                            <div className="col-12" id="loading">
                                <img src={Loading} alt="" />
                                
                            </div>
                            
                        </div>
                    </div>
                }

                
    		</div>
    	</div>
    </div>
  )
}

export default RoomEdit;