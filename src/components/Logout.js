import React, {useEffect, useState, useContext} from 'react';
import {Redirect} from 'react-router-dom';
import {AppContext} from './../AppProvider'

const Logout = () => {

	const [authUser, setAuthUser] = useContext(AppContext)

	const [isRedirect, setIsRedirect] = useState(false);

	useEffect(() => {
		localStorage.removeItem('appState')
		setAuthUser({
		    isAuth: false,
		    _id: "",
		    fullname: "",
		    email: ""
  		})
		setIsRedirect(true);
	
	},[]);

	if(isRedirect){
		return <Redirect to="/login" />
	}

  return (
    <div>
    	Logging out please wait a moment...
    </div>
  )
}

export default Logout;