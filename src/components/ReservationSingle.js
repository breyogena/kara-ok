import React, {useState, useEffect, useContext} from 'react';
import TableProduct from './room-partials/TableProduct';
import {useParams} from 'react-router-dom';
import ReservationHeader from './room-partials/ReservationHeader';
import Loading from './../images/loading.gif';
import {AppContext} from './../AppProvider'

const ReservationSingle = () => {

	const [authUser, setAuthUser] = useContext(AppContext)
	console.log(authUser)

	useEffect( () => {
    let appState = localStorage["appState"];

    if(appState) {
      fetch("https://karaokdb.herokuapp.com/users/profile",{
        headers: {
          "Authorization": `Bearer ${appState}`
        }
      })
      .then( response => response.json())
      .then( data => {
        
        if(data._id){
          
          setAuthUser({
            isAuth: true,
            _id: data._id,
            fullname: data.fullname,
            email: data.email,
            isAdmin: data.isAdmin
          })
        }
      })
    }
  }, []);

	const { id } = useParams();

	const [reservation, setReservation] = useState({
		_id: "",
		customerId: {
			fullname: ""
		}
		
	});

	const [isLoading, setIsLoading] = useState(true);

	useEffect(()=>{
		fetch(`https://karaokdb.herokuapp.com/reservations/${id}`, {
			headers: {
				"Authorization" : `Bearer ${localStorage["appState"]}`
			}
		})
		.then( res => res.json())
		.then( data => {
			if(data){
				setIsLoading(false);
			}
			setReservation(data)
		});
	},[]);

  return (
    <div className="container">
    	<div className="row">
    		<div className="col-12">
    			
    			{
    				!isLoading ?
    				<ReservationHeader reservation={reservation} authUser={authUser} />
    				: 
    				<div className="container" id="booking-loading">
							<div className="row">
								<div className="col-12" id="loading">
									<img src={Loading} alt="" />
									
								</div>
								
							</div>
						</div>
    			}

    		</div>


    		<div className="col-12">
	    		{
	    			reservation.orders ?
	    			<TableProduct orders={reservation.orders} />
	    			:
	    			<div className="container" id="booking-loading">
							<div className="row">
								<div className="col-12" id="loading">
									<img src={Loading} alt="" />
									
								</div>
								
							</div>
						</div>
	    		}
    		</div>
    	</div>
    </div>
  )
}

export default ReservationSingle;